import json
import math
import sys
import logging

logging.basicConfig(level=logging.INFO, format='[%(asctime)s][%(levelname)s] %(message)s')



logging.info("Starting application")

def decode_file(input_file):
    logging.info("Input file: "+input_file)
    # read file
    with open(input_file, 'r') as myfile:
        data=myfile.read()

    logging.debug("File content")
    logging.debug(data)

    # parse file
    obj = json.loads(data)
    logging.info("JSON decoded")
    return obj

obj = decode_file(sys.argv[2])
def pow(n):
    return math.pow(n,obj['pow'])

def encode_file(input_obj, output_file):
    logging.info("Output file: "+output_file)
    output = {
        "success": True,
        "spent": 0,
        "values": []
            }

    output['values'] = list(map(pow, input_obj['values']))

    logging.debug("Output object")
    logging.debug(output)

    json_output = json.dumps(output, indent = 4)

    with open(output_file, "w") as outfile:
        outfile.write(json_output)

def validate_obj(obj):
    if obj['pow'] <= 0:
        logging.error('invalid pow value')
        sys.exit(1)

    return True

if sys.argv[1] == 'compute':
    logging.info('Start computation')
    encode_file(obj, sys.argv[3])

if sys.argv[1] == 'validate':
    logging.info('Start validation')
    validate_obj(obj)

logging.info('Stopping application')
