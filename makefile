test:
	python src/main.py validate mock/bad_data.json output.json
	python src/main.py validate mock/data.json output.json
	python src/main.py compute mock/data.json output.json


init:
	kubectl config use-context docker-desktop
	kubectl apply -f manifests/namespace
	kubectl apply -f https://raw.githubusercontent.com/argoproj/argo-events/stable/manifests/install.yaml
	kubectl apply -f https://raw.githubusercontent.com/argoproj/argo-events/stable/manifests/install-validating-webhook.yaml
	kubectl apply -n argo-events -f https://raw.githubusercontent.com/argoproj/argo-events/stable/examples/eventbus/native.yaml

pf:
	kubectl -n argo port-forward deployment/argo-server 2746:2746

build:
	docker build -t workflow-demo:demo .
